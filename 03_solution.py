#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright 2017 Jacques Berger
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Basé sur le code de Jacques Berger disponible à l'adresse suivante :
# https://github.com/jacquesberger/exemplesINF3005/tree/master/Flask/login

# Ajout de commentaires par Luis-Gaylor Nobre.

from flask import Flask
from flask import render_template
from flask import request
from flask import redirect

app = Flask(__name__)


@app.route('/confirmation')
def confirmation_page():
    return render_template('confirmation.html')


@app.route('/', methods=["GET", "POST"])
def formulaire_creation():
    # Si le navigateur envoie une requête GET, il souhaite obtenir le formulaire
    if request.method == "GET":
        return render_template("formulaire.html")
    # Sinon, il utilise la méthode POST et envoie les données du formulaire
    # Avant de confirmer à l'usager que
    else:
        username = request.form["username"]
        password = request.form["password"]
        email = request.form["email"]
    # Vérification simple des champs non-vides.
    if username == "" or password == "" or email == "":
        return render_template("formulaire.html", error="Tous les champs sont obligatoires.")
    # Vous savez désormais comment envoyer une variable interprétable par Jinja2.
    # Comment validerez-vous le reste de vos champs ? N'oubliez pas que l'exercice vous demande de vérifier plusieurs types d'inputs.
    # Peut-être pourriez-vous faire un array ou un dictionnaire d'erreurs à passer au template formulaire.html ?
    else:
        return redirect("/confirmation")

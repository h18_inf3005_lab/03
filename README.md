# Étapes de résolution de ce laboratoire 03

### Installer Flask, Selon votre système d'exploitation :

1. Windows `pip install flask`
2. Sur Linux, `sudo pip install flask`
3. Sur Malt: `pip3 install --user flask`
4. Sur d'autres machines, essayez l'un ou l'autre ou référez-vous à la documentation:
http://flask.pocoo.org/docs/0.12/installation/


### Faites rouler un programme de base :
https://github.com/jacquesberger/exemplesINF3005/tree/master/Flask/hello_world

1. Sauvegardez le contenu de ce répertoire git sur votre machine locale. Attention ! Copiez bien le contenu RAW des fichiers... Sinon vous vous retrouvez avec la page html de git qui contient le code, si vous faites un clic droit et enregistrer le lien sous...
2. Jacques vous a fourni un makefile. Ouvrez-le et regardez les commandes.
	1. La première est `export FLASK_APP=index.py`
		2. Elle explique à Flask que vous souhaitez lancer l'application Flask appelée index.py qui se trouve dans ce répertoire
		3. Si vous utilisez Windows, il faudra remplacer `export` par `set`
    2. La deuxième est `flask run` qui lance l'application index.py.
	3. Pour faire rouler l'application vous pouvez directement utiliser la commande `make` dans votre terminal, lorsque vous vous êtes déplacé au sein du répertoire où se trouve vos deux fichiers.
	4. Vous pouvez aussi tout faire à la main!
		1. Essayez dans votre terminal la commande `export FLASK_APP=index.py` sur Linux/Mac ou `set FLASK_APP=index.py` sur Windows puis `flask run`
		2. Votre terminal devra vous donner la réponse suivante :
			1. \* Serving Flask app "index"
			2. \* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
		3. Ouvrez votre navigateur à l'adresse que vous donne votre terminal, en ajoutant bien les `:` avec le numéro de port qui suit. (5000 dans mon exemple plus haut)
3. Voilà, vous savez vous servir de Flask !

### Maintenant, essayez de comprendre les routes grâce à cet exemple
https://github.com/jacquesberger/exemplesINF3005/tree/master/Flask/form

1. Faites le rouler de la même manière que vous avez fait avec le programme précédent.
2. Le dossier templates est celui où se trouvent vos pages HTML. La racine (`/`) du site appelle le formulaire.html et lorsque le bouton submit est cliqué, il envoie vers la route ou adressse /merci
3. Flask reçoit les routes en question et envoie les pages qui sont spécifiées dans votre fichier Python.
4. Pour une explication rapide des principes de base de Flask, voir le petit tutoriel suivant:
https://www.tutorialspoint.com/flask/flask_templates.htm

### Apprenez à vous servir du debugger de Flask:
http://flask.pocoo.org/docs/0.12/quickstart/#debug-mode


### Essayez de coder votre propre validation en vous servant de son formulaire

1. Servez-vous à la fois de son exemple de formulaire et des lignes 57 à 76 du fichier index.py de l'exemple login de Jacques: https://github.com/jacquesberger/exemplesINF3005/tree/master/Flask/login
2. Utilisez le langage de templating Jinja2 pour afficher les erreurs dans votre formulaire en HTML
3. Pour avoir une bonne idée de la syntaxe de ce langage :
http://jinja.pocoo.org/docs/2.10/templates/

#### Si jamais vous aviez besoin d'un peu d'aide, je vous donne une solution partielle dans le fichier Python à la racine de ce répertoire

# C'est tout ! Bonne chance et à lundi !
